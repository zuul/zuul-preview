BUILDTYPE ?= Release

PREFIX := /usr/local
LDFLAGS := -lssl -lcrypto -lboost_system -lcpprest -pthread
WARNING_FLAGS := -pedantic -Werror -Wall -Wextra -Weffc++ -Wundef -Wshadow -Wstrict-aliasing -Wswitch-enum -Wformat=2 -Wattributes -Woverloaded-virtual -Wnon-virtual-dtor -Wctor-dtor-privacy -Wold-style-cast -Wconversion -Wframe-larger-than=32768 -Wredundant-decls
COMMON_FLAGS := -g -pthread -std=gnu++17 -fdiagnostics-show-option -fdiagnostics-generate-patch
RELEASE_FLAGS := -O2 -DNDEBUG
DEBUG_FLAGS := -O0 -DDEBUG -fno-inline-functions -fno-omit-frame-pointer

ifeq ($(BUILDTYPE),Release)
        CXXFLAGS := $(CXXFLAGS) $(COMMON_FLAGS) $(WARNING_FLAGS) $(RELEASE_FLAGS)
else
        CXXFLAGS := $(CXXFLAGS) $(COMMON_FLAGS) $(WARNING_FLAGS) $(DEBUG_FLAGS)
endif

all: zuul-preview

install: zuul-preview
	mkdir -p $(PREFIX)/bin
	cp zuul-preview $(PREFIX)/bin/zuul-preview

uninstall:
	rm $(PREFIX)/bin/zuul-preview

zuul-preview: main.o
	g++ -o $@ $^ $(LDFLAGS)

%.o: %.cc
	g++ $(CXXFLAGS) -c -o $@ $<

clean:
	rm -f ./zuul-preview *.o
